<?php

class DefaultController extends ApiBaseController
{
	public function actionIndex()
	{
		echo "*^_^*";
	}
    
    public function actionWeather($id){
        if(!preg_match('/^\d*$/', $id)){
            $id = $this->getWeatherIdByName($id);
        }
        $data = $this->getWeather($id);
        echo json_encode($data);
    }
    
    private function getWeather($id){
        $sql = "SELECT weather FROM {{weather}} WHERE weather_id=:weather_id";
        $row = Yii::app()->db->createCommand($sql)->bindValue(":weather_id",$id)->queryRow();
        return json_decode($row['weather']);
    }
    
    private function getWeatherIdByName($name){
        $sql = "SELECT weather_id FROM {{china}} WHERE name=:name AND name_type='station'";
        $cmd = Yii::app()->db->createCommand($sql);
        $row = $cmd->bindValue(":name",$name)->queryRow();
        return $row ? $row['weather_id'] : false;
    }
}